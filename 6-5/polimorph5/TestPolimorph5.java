package polimorph5;
public class TestPolimorph5 {
    public static void main(String[] args) {
        Circle c1 = new Circle(21);
        System.out.println(c1);
        System.out.println("Area "+c1.getArea());
        System.out.println("Perimeter "+c1.getPerimeter());

        ResizeableCircle rc = new ResizeableCircle(14);
        System.out.println(rc);
        rc.resize(50);
        System.out.println("Resized 50% "+rc);
    }
}
