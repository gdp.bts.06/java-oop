package polimorph2;
public class ShapeInterfaceTest {
    public static void main(String[] args) {
        Circle c1 = new Circle(7);
        System.out.println(c1);
        System.out.println("Area: "+c1.getArea());
        System.out.println("Perimeter: "+c1.getPerimeter());

        Rectangle r1 = new Rectangle(10, 20);
        System.out.println(r1);
        System.out.println("area: "+r1.getArea());
        System.out.println("perimeter:  "+r1.getPerimeter());
    }
}
