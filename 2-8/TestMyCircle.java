import java.util.Arrays;

public class TestMyCircle {
    public static void main(String[] args) {
        MyPoint p2 = new MyPoint(3,3);
        MyCircle c0 = new MyCircle();
        MyCircle c1 = new MyCircle(4,4,7);
        MyCircle c2 = new MyCircle(2,4,7);
        MyCircle c3 = new MyCircle(4,2,7);
        System.out.println(c0);
        System.out.println(c1);
        System.out.println(c1.getCenterX());
        System.out.println(c1.getCenterY());
        System.out.println(Arrays.toString(c1.getCenterXY()));
        System.out.println(c2);
        System.out.println(c2.getRadius());
        System.out.println(c3.getArea());

        c1.setCenterXY(2, 1);
        c2.setRadius(8);
        c2.setCenter(p2);
        c3.setCenterX(1);
        c3.setCenterY(1);

        System.out.println(c0);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c1.distance(c2));
        System.out.println(c1.distance(c3));
        System.out.println(c2.distance(c3));
        System.out.println(c1.distance(c1));
    }
}
