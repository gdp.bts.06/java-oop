package inheritance6;

public class TestAnimal {
    public static void main(String[] args) {
        Animal a1 = new Animal("Blacky");
        System.out.println(a1);

        Mammal m1 = new Mammal("Sandy");
        System.out.println(m1);

        Cat c1 = new Cat("Jimbon");
        System.out.println(c1);
        c1.greets();

        Dog d1 = new Dog("Chase");
        System.out.println(d1);
        d1.greets();
        Dog d2 = new Dog("Apolo");
        System.out.println(d2);
        d2.greets(d1);
    }
}
