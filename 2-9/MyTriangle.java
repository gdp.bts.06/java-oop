public class MyTriangle {
    private MyPoint v1, v2, v3;

    MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
        v1  = new MyPoint(x1,y1);
        v2  = new MyPoint(x2,y2);
        v3  = new MyPoint(x3,y3);
    }

    MyTriangle(MyPoint v1, MyPoint v2, MyPoint v3){
        this.v1  = v1;
        this.v2  = v2;
        this.v3  = v3;
    }

    @Override
    public String toString() {
        return "My Triangle[v1= "+v1+", v2= "+v2+", v3="+v3+"]";
    }

    public double getPerimeter(){
        return v1.distance()+v2.distance()+v3.distance();
    }

    public String getType(){
        double s1 = v1.distance(),
               s2 = v2.distance(),
               s3 = v3.distance(); 
        if (s1==s2 && s1==s3) {
            return "equilateral";
        } else if(s1==s2 || s1==s3 || s2 == s3) {
            return "isosceles";
        }else{
            return "scalene";
        }
    }
}
