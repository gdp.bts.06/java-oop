public class TestMyTriangle {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(2,2);
        MyPoint p2 = new MyPoint(3,3);
        MyPoint p3 = new MyPoint(5,5);
        MyTriangle t1 = new MyTriangle(4,4,4,4,4,4);
        MyTriangle t2 = new MyTriangle(p1,p2,p2);
        MyTriangle t3 = new MyTriangle(p1,p2,p3);

        System.out.println(t1);
        System.out.println("Type : "+t1.getType());
        System.out.println("Perimeter : "+t1.getPerimeter());

        System.out.println(t2);
        System.out.println("Type : "+t2.getType());
        System.out.println("Perimeter : "+t2.getPerimeter());

        System.out.println(t3);
        System.out.println("Type : "+t3.getType());
        System.out.println("Perimeter : "+t3.getPerimeter());
    }
}
