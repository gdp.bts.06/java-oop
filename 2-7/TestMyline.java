public class TestMyline {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(1,3);
        System.out.println(p1);
        MyPoint p2 = new MyPoint(4,4);
        System.out.println(p2);

        MyLine l2 = new MyLine(2, 2, 3, 4);
        System.out.println(l2);
        MyLine l1 = new MyLine(p1, p2);
        System.out.println(l1);
        System.out.println(l1.getGradient());
        System.out.println(l1.getLength());
    }
}
