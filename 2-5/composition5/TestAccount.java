package composition5;
public class TestAccount {
    public static void main(String[] args) {
        Customer c1 = new Customer(1, "Udin", 10);
        System.out.println(c1);

        Account a1 = new Account(1, c1, 1000.699);
        System.out.println(a1);
        a1.setBalance(500);
        System.out.println("ID:"+a1.getID());
        System.out.println("Name: "+a1.getCustomerName());
        System.out.println("Balance "+a1.getBalance());
        System.out.println(a1.deposit(500.555));
        System.out.println(a1.withdraw(200.11));
        System.out.println(a1.withdraw(2000.11));
    }
}
