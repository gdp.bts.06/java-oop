package inheritance4;
public class Point {
    private float x, y;

    Point(){
        x = 0.0f;
        y = 0.0f;
    }

    Point(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
    
    public void setXY(float x, float y){
        this.x=x;
        this.y=y;
    }

    public float[] getXY(){
        float[] xy ={this.x,this.y};
        return xy;
    }

    public String toString(){
        return "("+x+","+y+")";
    }
}
