package anotherOOP;
import java.util.Date;


public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense, productExpense;

    public Visit(String name, Date date){
        customer = new Customer(name);
        this.date = date;
    }

    public Visit(Customer customer, Date date){
        this.customer = customer;
        this.date = date;
    }

    public String getName(){
        return customer.getName();
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense(){
        if (customer.isMember()) {
            serviceExpense -= serviceExpense * DiscountRate.getServiceDiscountRate(customer.getMemberType());
            productExpense -= productExpense * DiscountRate.getProductDiscountRate(customer.getMemberType());
        }
        return serviceExpense + productExpense;
    }

    public String toString() {
        return customer
            + "\n visit date: "+date
            + "\n service expense: "+serviceExpense
            + "\n product expense: "+productExpense
            + "\n total expense: "+getTotalExpense()
        ;
    }
}
