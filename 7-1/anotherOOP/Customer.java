package anotherOOP;

public class Customer {
    private String name, memberType;
    private boolean member;

    public Customer(String name){
        this.name = name;
        member = false;
    }

    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String toString() {
        if (member) {
            return "Member[Name: "+name+", Member: "+member+", MemberType: "+memberType+"]";
        }
        return "Member[Name: "+name+", Member: "+member+"]";
    }
}
