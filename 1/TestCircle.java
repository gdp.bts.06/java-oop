public class TestCircle {
    public static void main(String[] args) {
        Circle c4 = new Circle();   // construct an instance of Circle
        c4.setRadius(5.5);          // change radius
        System.out.println("radius is: " + c4.getRadius()); // Print radius via getter
        c4.setColor("green");       // Change color
        System.out.println("color is: " + c4.getColor());   // Print color via getter

        c4.setRadius(4.4);
        System.out.println(c4.getRadius());
        
        // call to string explicit
        Circle c5 = new Circle(5.5);
        System.out.println(c5.toString());

        Circle c6 = new Circle(6.6);
            System.out.println(c6.toString());  // explicit call
            System.out.println(c6);             // println() calls toString() implicitly, same as above
            System.out.println("Operator '+' invokes toString() too: " + c6);  // '+' invokes toString() too
        }
}
