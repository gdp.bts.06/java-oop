package polimorph6;
public abstract class Animal {
    private String name;

    public Animal(String name){
        this.name = name;
    }
    
    public String toString() {
        return "Animal[name = "+name+"]";
    }

    abstract public void greets();
}
